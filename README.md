# Tampermonkey script for Braintree

## Change Log ##

### 0.7
+ Billing street address is now a required field

### 0.6
+ Validate customer email address and block anything which matches the SHA-256

### 0.5
+ Display customer email as a required field
+ Display customer phone as a required field
+ Display billing street address as a non-required field

### 0.4
+ Update qualifying domains

### 0.3
+ Remove `var title` as it is deprecated

### 0.2
+ Remove auto tax calculation

### 0.1
+ Initial build