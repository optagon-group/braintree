// ==UserScript==
// @name         Braintree
// @namespace    https://www.optagongroup.co.uk/
// @version      0.7
// @description  Refine the UI of Braintree
// @author       Dan Norris
// @include      /^https://(www|sandbox)\.braintreegateway\.com\/merchants\/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    //var title = document.getElementsByTagName("title")[0].innerHTML;

    function setStatusColours(status) {

        var statusPaddingLeft = "8px",
            statusPaddingRight = "8px",
            statusBackgroundColor = "",
            statusColor = "";

        if (status == "negative") {
            statusBackgroundColor = "#ee1600";
            statusColor = "#FFFFFF";
        } else if ("positive") {
            statusBackgroundColor = "#CAE4CE";
            statusColor = "#1e8a62";
        } else {
            statusPaddingLeft = "";
            statusPaddingRight = "";
        }

        let statusItems = document.getElementsByClassName(status);
        if(statusItems.length) {
            for (i = 0; i < statusItems.length; i++) {
                var span = statusItems[i].getElementsByTagName("span");
                if(span.length) {
                    span[0].style.backgroundColor = statusBackgroundColor;
                    span[0].style.color = statusColor;
                    span[0].style.paddingLeft = statusPaddingLeft;
                    span[0].style.paddingRight = statusPaddingRight;
                }
            }
        }
    }

    function propChecked(element) {

        var elementObject = document.getElementById("toggleable_address_source_new")
        if(elementObject) {
            elementObject.checked = true;
        }

    }

    function setHelp(element, text) {

        var elementObject = document.getElementById(element),
            elementObjectTag = document.createElement("p"),
            elementObjectText = document.createTextNode(text);

        elementObjectTag.setAttribute("class","inform help");
        elementObjectTag.appendChild(elementObjectText);
        elementObject.parentNode.insertBefore(elementObjectTag, elementObject.nextSibling);

    }

    /**
     * @deprecated since 0.2
     */
    function getTaxAmount() {

        var transactionAmount = document.getElementById("transaction_amount"),
            transactionTaxAmount = document.getElementById("transaction_tax_amount");

        if(transactionAmount && transactionTaxAmount) {
            transactionAmount.addEventListener('input', function(){ transactionTaxAmount.value = calculateTax(transactionAmount)});
        }

    }

    /**
     * @deprecated since 0.2
     */
    function calculateTax(element) {

        element.value = element.value.replace(/[^0-9\.]+/g,'');
        if(element.value) {
            var exclVat = element.value / 1.2,
                vat = element.value - exclVat;
            return Math.round((vat + Number.EPSILON) * 100) / 100;
        } else {
            return '0';
        }

    }

    /**
     * SHA-256 encode
     * @see https://developer.mozilla.org/en-US/docs/Web/API/SubtleCrypto/digest
     */
    async function digestMessage(message) {
        const msgUint8 = new TextEncoder().encode(message); // encode as (utf-8) Uint8Array
        const hashBuffer = await crypto.subtle.digest('SHA-256', msgUint8); // hash the message
        const hashArray = Array.from(new Uint8Array(hashBuffer)); // convert buffer to byte array
        const hashHex = hashArray.map((b) => b.toString(16).padStart(2, '0')).join(''); // convert bytes to hex string
        return hashHex;
    }

    /**
     * Prevent sale if customer email SHA-256 is in the blocked array
     * For a list of decoded hashes see Google Sheet "TamperMonkey : Braintree : Blocked Customers"
     */
    function blockCustomerEmail() {
        let fieldEmail = document.getElementById("transaction_customer_email");
        let btnCreateTransaction = document.getElementById("create_transaction_btn");
        let blockedCustomers = [
            '87d74bbb4346d1ddff715c26f376e3c07558146432e722c418b0cefa4cfc8a74'
        ]

        fieldEmail.addEventListener("blur", function(event){
            var emailAddress = event.target.value.toLowerCase().trim();
            digestMessage(emailAddress).then(function(emailAddressSHA) {
                if(blockedCustomers.includes(emailAddressSHA)) {
                    btnCreateTransaction.disabled = true;
                    alert("!!! FRAUD PROTECTION !!! \n \n DO NOT SELL TO THIS CUSTOMER ");
                } else {
                    btnCreateTransaction.disabled = false;
                }
            });
        });
    }

    let headerGroup = document.getElementsByClassName("header_group");
    if(headerGroup.length) {

        let heading = headerGroup[0].innerText;

        if(heading.startsWith('New Transaction')) {

            /*
             * Hide unnecessary elements
             */
            let fieldsHidden = [
                'transaction_purchase_order_number_container',
                'transaction_type_container',
                'transaction_tax_amount_container',
                'transaction_options_submit_for_settlement_container',
                'currency_iso_code',
                'transaction_options_skip_advanced_fraud_checking_container',
                'transaction_options_store_in_vault_on_success_container',
                'transaction_customer_fax_container',
                'transaction_customer_website_container',
                'transaction_customer_company_container',
                'transaction_billing_first_name_container',
                'transaction_billing_last_name_container',
                'transaction_billing_extended_address_container',
                'transaction_billing_company_container',
                'transaction_billing_locality_container',
                'transaction_billing_region_container',
                'transaction_options_add_billing_address_to_payment_method_container',
                'toggleable_address_source_none',
                'toggleable_address_source_new'
            ];
            fieldsHidden.forEach(function(item, index, array) {
                var elementObject = document.getElementById(item);
                if(elementObject) {
                    elementObject.parentElement.style.display = 'none';
                }
            });

            /*
             * Set help messaging for Order ID
             */
            setHelp("transaction_order_id_container", "QuickBooks Invoice ID");
            setHelp("transaction_amount_container", "Including VAT");

            /*
             * Force new required elements
             */
            let fieldsRequired = [
                'transaction_customer_first_name',
                'transaction_customer_last_name',
                'transaction_customer_email',
                'transaction_order_id',
                'transaction_credit_card_cvv',
                'transaction_billing_street_address',
                'transaction_billing_postal_code'
            ];

            fieldsRequired.forEach(function(item, index, array) {

                var elementRequired = document.createElement("span"),
                    elementRequiredText = document.createTextNode("Required"),
                    elementItem = document.getElementById(item);

                elementRequired.setAttribute("class","required");
                elementRequired.appendChild(elementRequiredText)

                if(elementItem) {
                    elementItem.setAttribute('required','required');
                    elementItem.parentNode.insertBefore(elementRequired, elementItem.nextSibling);
                }
            });

            /*
             * Block specific customers
             */
            blockCustomerEmail();

            /*
             * Calculate Tax
             */
            //getTaxAmount();

            /*
             * Manipulate billing address
             */
            propChecked("toggleable_address_source_new");
            propChecked("transaction_options_add_billing_address_to_payment_method");

            /*
             * Refine the country select to be accepted countries only and set default value as UK
             */
            var selectobject = document.getElementById("transaction_billing_country_name");
            if (selectobject) {
                let allowedCountries = [
                    'Guernsey',
                    'Ireland',
                    'Isle of Man',
                    'Jersey',
                    'United Kingdom'
                ];

                var l = selectobject.length;
                for (var i=0; i < l; i++) {
                    var country = selectobject.options[i].value;
                    if ( ! allowedCountries.includes(country)) {
                        selectobject.removeChild(selectobject.options[i]);
                        i--;
                        l--;
                    }
                }
                selectobject.value = "United Kingdom";
                selectobject.previousSibling.textContent = selectobject.value;
            }

            /*
             * Manipulate shipping address
             */
            propChecked("transaction_shipping_address_source_none");
            for (const h3 of document.querySelectorAll("h3")) {
                if (h3.textContent.includes("Shipping Address")) {
                    h3.parentElement.style.display = 'none';
                }
            }

        }

        if(heading.startsWith('Transaction Detail For ID:')) {
            setStatusColours("negative");
            setStatusColours("positive");
        }

    }



})();